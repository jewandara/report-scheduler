﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Threading;
using System.Windows.Media;

namespace WpfApp1
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            InitializeMainThread();
        }


        #region INITIALIZING BOTS VARIABLES DEFINITION
        /***********************************************************/
        /******** INITIALIZING BOTS VARIABLES DEFINITION ***********/
        /***********************************************************/
        public class Bots
        {
            public string BotId { get; set; }
            public string BotName { get; set; }
            public string RunType { get; set; }
            public string WeekDay { get; set; }
            public int RunDate { get; set; }
            public int RunHour { get; set; }
            public int RunMinit { get; set; }
            public string BotPath { get; set; }
            public string BotDecription { get; set; }
            public string BotNextRunTime { get; set; }
            public string BotWaitingTime { get; set; }
            public Boolean IsRunTime { get; set; }
            public String BotVisable { get; set; }
            public String BotNotVisable { get; set; }
            public String BotEnableMode { get; set; }
            public String BotEnableColor { get; set; }
            public int BotProgress { get; set; }
            public Boolean RunningBotCancellation { get; set; }
            public String RunningBotCmdColor { get; set; }
            public int RunningBotNumber { get; set; }
            public int RunningBotLines { get; set; }
            public Thread RunningBotThread { get; set; }
        }
        public int RUNNING_BOT_LIST_ID = 0;
        public Dictionary<String, Bots> BOT_LIST = new Dictionary<String, Bots>();
        public Dictionary<String, Bots> RUNNING_BOT_LIST = new Dictionary<String, Bots>();
        public int RUNNING_BOTS_CMD_LINES = 0;
        public int RUNNING_BOTS_PROGRESS_BAR = 0;
        public String[] BOT_LABEL_COLOURS = { "LightGreen", "LightPink", "LightBlue", "LightCoral", "LightCyan", "LightYellow",  "Green", "Red", "Blue", "Cyan", "Green", "Yellow" };
        public String[] WEEK_ARRY = { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };
        public String[] MONTH_ARRY = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
        public String[] MONTHDAY_ARRY = { "First", "Second", "Third", "Fourth", "Fifth", "Sixth", "Seventh", "Eighth", "Ninth", "Tenth", "Eleventh", "Twelfth", "Thirteenth", "Fourteenth", "Fifteenth", "Sixteenth", "Seventeenth", "Eighteenth", "Nineteenth", "Twentieth", "Twenty-first", "Twenty-second", "Twenty-third", "Twenty-fourth", "Twenty-fifth", "Twenty-sixth", "Twenty-seventh", "Twenty-eighth", "Twenty-ninth", "Thirtieth", "Thirty-first", "Thirty-second", "Thirty-third" };
        /***********************************************************/
        #endregion


        #region BOTS TIMING FUNCTION 
        /***********************************************************/
        /********    INITIALIZING BOTS TIMING FUNCTION   ***********/
        /***********************************************************/
        private DateTime FindNextDate(String NAME)
        {
            DateTime today = DateTime.Today;
            int NO = 0;
            if (NAME == "Monday") { NO = ((int)DayOfWeek.Monday - (int)today.DayOfWeek + 7) % 7; }
            else if (NAME == "Tuesday") { NO = ((int)DayOfWeek.Tuesday - (int)today.DayOfWeek + 7) % 7; }
            else if (NAME == "Wednesday") { NO = ((int)DayOfWeek.Wednesday - (int)today.DayOfWeek + 7) % 7; }
            else if (NAME == "Thursday") { NO = ((int)DayOfWeek.Thursday - (int)today.DayOfWeek + 7) % 7; }
            else if (NAME == "Friday") { NO = ((int)DayOfWeek.Friday - (int)today.DayOfWeek + 7) % 7; }
            else if (NAME == "Saturday") { NO = ((int)DayOfWeek.Saturday - (int)today.DayOfWeek + 7) % 7; }
            else if (NAME == "Sunday") { NO = ((int)DayOfWeek.Sunday - (int)today.DayOfWeek + 7) % 7; }
            else { NO = 0; }
            return today.AddDays(NO);
        }
        private String FindWaittingTimeDifference(DateTime NEW_NEXT_RUN_DATE_AND_TIME)
        {
            TimeSpan DIFFERENCE = (NEW_NEXT_RUN_DATE_AND_TIME - DateTime.Now);
            String WAITING_TIME;
            if (DIFFERENCE.Days == 0)
            {
                if (DIFFERENCE.Hours == 0)
                {
                    if (DIFFERENCE.Minutes == 0) { WAITING_TIME = DIFFERENCE.Seconds.ToString() + " Seconds"; }
                    else { WAITING_TIME = DIFFERENCE.Minutes.ToString() + " Minutes and " + DIFFERENCE.Seconds.ToString() + " Seconds"; }
                }
                else { WAITING_TIME = DIFFERENCE.Hours.ToString() + " Hours, " + DIFFERENCE.Minutes.ToString() + " Minutes and " + DIFFERENCE.Seconds.ToString() + " Seconds"; }
            }
            else { WAITING_TIME = DIFFERENCE.Days.ToString() + " Days, " + DIFFERENCE.Hours.ToString() + " Hours, " + DIFFERENCE.Minutes.ToString() + " Minutes and " + DIFFERENCE.Seconds.ToString() + " Seconds"; }
            return WAITING_TIME;
        }
        private DateTime FindDayRunTime(Bots BOT_TIME_DATA)
        {
            var Today = DateTime.Now;
            var Tomorrow = Today.AddDays(1);
            var DayAfterTomorrow = Today.AddDays(2);
            var TodayName = Today.ToString("dddd");
            var TomorrowName = Tomorrow.ToString("dddd");

            int CON_HH = BOT_TIME_DATA.RunHour;
            int CON_MM = BOT_TIME_DATA.RunMinit;
            int TOTAL_CON_MM = (CON_HH * 60) + CON_MM;

            DateTime CURRENT_TIME = DateTime.ParseExact(DateTime.Now.ToString("MM/dd/yy HH:mm"), "MM/dd/yy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
            int CUR_HH = CURRENT_TIME.Hour;
            int CUR_MM = CURRENT_TIME.Minute;
            int TOTAL_CUR_MM = (CUR_HH * 60) + CUR_MM;

            DateTime NEW_NEXT_RUN_DATE_AND_TIME;
            if (BOT_TIME_DATA.RunType == "WEEK")
            {
                if (TodayName == BOT_TIME_DATA.WeekDay)
                {
                    if (TOTAL_CON_MM > TOTAL_CUR_MM) { NEW_NEXT_RUN_DATE_AND_TIME = new DateTime(Today.Year, Today.Month, Today.Day, CON_HH, CON_MM, 00); }
                    else
                    {
                        NEW_NEXT_RUN_DATE_AND_TIME = FindNextDate(BOT_TIME_DATA.WeekDay);
                        NEW_NEXT_RUN_DATE_AND_TIME = new DateTime(NEW_NEXT_RUN_DATE_AND_TIME.Year, NEW_NEXT_RUN_DATE_AND_TIME.Month, NEW_NEXT_RUN_DATE_AND_TIME.Day, CON_HH, CON_MM, 00);
                    }
                }
                else
                {
                    NEW_NEXT_RUN_DATE_AND_TIME = FindNextDate(BOT_TIME_DATA.WeekDay);
                    NEW_NEXT_RUN_DATE_AND_TIME = new DateTime(NEW_NEXT_RUN_DATE_AND_TIME.Year, NEW_NEXT_RUN_DATE_AND_TIME.Month, NEW_NEXT_RUN_DATE_AND_TIME.Day, CON_HH, CON_MM, 00);
                }
            }
            else if (BOT_TIME_DATA.RunType == "MONTH")
            {
                if (BOT_TIME_DATA.RunDate == Today.Day)
                {
                    if (TOTAL_CON_MM > TOTAL_CUR_MM) { NEW_NEXT_RUN_DATE_AND_TIME = new DateTime(Today.Year, Today.Month, Today.Day, CON_HH, CON_MM, 00); }
                    else
                    {
                        NEW_NEXT_RUN_DATE_AND_TIME = new DateTime(Today.Year, Today.Month, BOT_TIME_DATA.RunDate, CON_HH, CON_MM, 00);
                        NEW_NEXT_RUN_DATE_AND_TIME = NEW_NEXT_RUN_DATE_AND_TIME.AddMonths(1);
                    }
                }
                else if (BOT_TIME_DATA.RunDate > Today.Day) { NEW_NEXT_RUN_DATE_AND_TIME = new DateTime(Today.Year, Today.Month, BOT_TIME_DATA.RunDate, CON_HH, CON_MM, 00); }
                else
                {
                    NEW_NEXT_RUN_DATE_AND_TIME = new DateTime(Today.Year, Today.Month, BOT_TIME_DATA.RunDate, CON_HH, CON_MM, 00);
                    NEW_NEXT_RUN_DATE_AND_TIME = NEW_NEXT_RUN_DATE_AND_TIME.AddMonths(1);
                }
            }
            else
            {
                if (TodayName == BOT_TIME_DATA.WeekDay) { NEW_NEXT_RUN_DATE_AND_TIME = new DateTime(Tomorrow.Year, Tomorrow.Month, Tomorrow.Day, CON_HH, CON_MM, 00); }
                else if (TOTAL_CON_MM > TOTAL_CUR_MM) { NEW_NEXT_RUN_DATE_AND_TIME = new DateTime(Today.Year, Today.Month, Today.Day, CON_HH, CON_MM, 00); }
                else
                {
                    NEW_NEXT_RUN_DATE_AND_TIME = (TomorrowName == BOT_TIME_DATA.WeekDay) ? DayAfterTomorrow : Tomorrow;
                    NEW_NEXT_RUN_DATE_AND_TIME = new DateTime(NEW_NEXT_RUN_DATE_AND_TIME.Year, NEW_NEXT_RUN_DATE_AND_TIME.Month, NEW_NEXT_RUN_DATE_AND_TIME.Day, CON_HH, CON_MM, 00);
                }
            }
            return NEW_NEXT_RUN_DATE_AND_TIME;
        }
        private Boolean IsThisRunTime(DateTime BOT_DATE_TIME)
        {
            var TODAY = DateTime.Now;
            TODAY = TODAY.AddSeconds(10);
            if ((BOT_DATE_TIME.Year == TODAY.Year) && (BOT_DATE_TIME.Month == TODAY.Month) && (BOT_DATE_TIME.Date == TODAY.Date) && (BOT_DATE_TIME.Hour == TODAY.Hour) && (BOT_DATE_TIME.Minute == TODAY.Minute)) { return true; }
            else { return false; }
        }
        /***********************************************************/
        #endregion


        #region INITIALIZING RUNNING CMD PROMPT WINDOW
        /***********************************************************/
        /******** INITIALIZING RUNNING CMD PROMPT WINDOW ***********/
        /***********************************************************/
        private Brush SetColorToCmdView(String COLOR)
        {
            switch (COLOR)
            {
                case "LightGreen": return Brushes.LightGreen;
                case "LightPink": return Brushes.LightPink;
                case "LightBlue": return Brushes.LightBlue;
                case "LightCoral": return Brushes.LightCoral;
                case "LightCyan": return Brushes.LightCyan;
                case "LightYellow": return Brushes.LightYellow;
                case "LightSalmon": return Brushes.LightSalmon;
                case "Green": return Brushes.Green;
                case "Red": return Brushes.Red;
                case "Blue": return Brushes.Blue;
                case "Cyan": return Brushes.Cyan;
                case "Yellow": return Brushes.Yellow;
                default: return Brushes.LightGreen;
            }
        }
        private void ProgressBarRunningBot()
        {
            if (RUNNING_BOTS_PROGRESS_BAR >= 100) { RUNNING_BOTS_PROGRESS_BAR = 0; }
            else { RUNNING_BOTS_PROGRESS_BAR = RUNNING_BOTS_PROGRESS_BAR + 1; }
        }
        private void SetTextToCmdView(String BOT_NAME, String BOT_OUTPUT)
        {
            if (RUNNING_BOT_LIST.ContainsKey(BOT_NAME))
            {
                Paragraph NewParagraph = new Paragraph();
                NewParagraph.FontSize = 12;
                NewParagraph.Foreground = SetColorToCmdView(RUNNING_BOT_LIST[BOT_NAME].RunningBotCmdColor);
                NewParagraph.Inlines.Add(new Bold(new Run(RUNNING_BOTS_CMD_LINES.ToString() + "   " + DateTime.Now.ToString("yyyy-MM-dd hh:mm tt") + "   |   " + BOT_NAME + " (" + RUNNING_BOT_LIST[BOT_NAME].RunningBotNumber + ")   :   ")));
                NewParagraph.Inlines.Add(BOT_OUTPUT);
                RUNNING_BOT_LIST[BOT_NAME].RunningBotLines++;
                RUNNING_BOTS_CMD_LINES++;
                this.CmdPromptViewLabele.Document.Blocks.Add(NewParagraph);
                if (RUNNING_BOTS_CMD_LINES > 1200)
                { this.CmdPromptViewLabele.Document.Blocks.Remove(this.CmdPromptViewLabele.Document.Blocks.FirstBlock); }
                this.CmdPromptViewLabele.ScrollToEnd();
            }
        }
        private void LoadingNewBotToRun(Bots NEW_BOT_DATA)
        {
            if (!RUNNING_BOT_LIST.ContainsKey(NEW_BOT_DATA.BotName))
            {
                Thread NEW_BOT_THREAD = new Thread(CallToRunPythonBot);
                NEW_BOT_DATA.RunningBotCancellation = false;
                NEW_BOT_DATA.RunningBotCmdColor = BOT_LABEL_COLOURS[RUNNING_BOT_LIST_ID];
                NEW_BOT_DATA.RunningBotNumber = RUNNING_BOT_LIST_ID;
                NEW_BOT_DATA.RunningBotLines = 0;
                NEW_BOT_DATA.RunningBotThread = NEW_BOT_THREAD;
                RUNNING_BOT_LIST.Add(NEW_BOT_DATA.BotName, NEW_BOT_DATA);
                RUNNING_BOT_LIST_ID = (RUNNING_BOT_LIST_ID > 10) ? 0 : RUNNING_BOT_LIST_ID + 1;
                //NEW_BOT_THREAD.Start(NEW_BOT_DATA.BotPath);
                NEW_BOT_THREAD.Start(NEW_BOT_DATA);
            }
        }
        private void UnLoadingRunningBotToFinish(String OLD_BOT_NAME)
        {
            //Thread OLD_BOT_THREAD = new Thread(new ThreadStart(ClearBot));
            //OLD_BOT_THREAD.Start(OLD_BOT_NAME); 
            ThreadPool.QueueUserWorkItem(new WaitCallback(ClearBot), OLD_BOT_NAME);
        }
        private void StartBot(String BOT_NAME)
        {
            if (BOT_LIST.ContainsKey(BOT_NAME))
            {
                Console.Out.WriteLine("GOOD : " + BOT_NAME);
                if (!RUNNING_BOT_LIST.ContainsKey(BOT_NAME))
                {
                    Console.Out.WriteLine("STARTING : "+BOT_NAME);
                    BOT_LIST[BOT_NAME].IsRunTime = true;
                    BOT_LIST[BOT_NAME].BotWaitingTime = " . . . ";
                    BOT_LIST[BOT_NAME].BotVisable = "Visible";
                    BOT_LIST[BOT_NAME].BotNotVisable = "Collapsed";
                    BOT_LIST[BOT_NAME].BotEnableMode = "True";
                    BOT_LIST[BOT_NAME].BotEnableColor = "Red";
                    BOT_LIST[BOT_NAME].BotProgress = 0;
                    LoadingNewBotToRun(BOT_LIST[BOT_NAME]);
                }
            }
            //ThreadPool.QueueUserWorkItem(new WaitCallback(CallToRunPythonBot), BOT_NAME); 
        }
        private void ClearBot(Object BOT_NAME)
        {
            if (RUNNING_BOT_LIST.ContainsKey(BOT_NAME.ToString()))
            {
                RUNNING_BOT_LIST[BOT_NAME.ToString()].RunningBotThread.Abort(BOT_NAME.ToString());
                RUNNING_BOT_LIST.Remove(BOT_NAME.ToString());
                Thread.Sleep(5000);
            }
        }
        /***********************************************************/
        #endregion


        #region INITIALIZING RUNNING MAIN THREAD
        /***********************************************************/
        /**********   INITIALIZING RUNNING MAIN THREAD  ************/
        /***********************************************************/
        private void InitializeMainThread()
        {
            Thread MainThread = new Thread(new ThreadStart(ReadingBotsLgFile));
            MainThread.Start();
        }
        private void ReadingBotsLgFile()
        {
            try
            {
                while (true)
                {
                    BOT_LIST.Clear();
                    var docPath = Directory.GetCurrentDirectory();
                    string[] _BOTS = File.ReadAllLines("bots.lg");
                    ObservableCollection<Bots> ScheduleItems = new ObservableCollection<Bots>();
                    foreach (string _DATA in _BOTS)
                    {
                        Bots MplsBots = JsonConvert.DeserializeObject<Bots>(_DATA.ToString());
                        DateTime BotNextRunTime = FindDayRunTime(MplsBots);
                        MplsBots.BotNextRunTime = BotNextRunTime.ToString("f", System.Globalization.CultureInfo.CreateSpecificCulture("en-US")); //Console.WriteLine(MplsBots.BotNextRunTime);
                        MplsBots.BotWaitingTime = FindWaittingTimeDifference(BotNextRunTime); //Console.WriteLine(MplsBots.BotWaitingTime);
                        if (RUNNING_BOT_LIST.ContainsKey(MplsBots.BotName))
                        {
                            MplsBots.IsRunTime = true;
                            MplsBots.BotWaitingTime = "until the this running bot is finished";
                            ProgressBarRunningBot();
                            MplsBots.BotVisable = "Visible";
                            MplsBots.BotNotVisable = "Collapsed";
                            MplsBots.BotEnableMode = "True";
                            MplsBots.BotEnableColor = "Red";
                            MplsBots.BotProgress = RUNNING_BOTS_PROGRESS_BAR; 
                            ScheduleItems.Add(MplsBots);
                            ScheduleItems.Move(ScheduleItems.IndexOf(MplsBots), 0);
                        }
                        else
                        {
                            if (IsThisRunTime(BotNextRunTime))
                            {
                                MplsBots.IsRunTime = true;
                                MplsBots.BotWaitingTime = " . . . ";
                                MplsBots.BotVisable = "Visible";
                                MplsBots.BotNotVisable = "Collapsed";
                                MplsBots.BotEnableMode = "True";
                                MplsBots.BotEnableColor = "Red";
                                MplsBots.BotProgress = 0;
                                LoadingNewBotToRun(MplsBots);
                            }
                            else
                            {
                                MplsBots.IsRunTime = false;
                                MplsBots.BotVisable = "Collapsed";
                                MplsBots.BotNotVisable = "Visible";
                                MplsBots.BotEnableMode = "True";
                                MplsBots.BotEnableColor = "Black";
                                MplsBots.BotProgress = 0;
                            }
                            ScheduleItems.Add(MplsBots);
                        }
                        //ScheduleItems.Add(MplsBots);
                        //ScheduleItems.Move(0, ScheduleItems.IndexOf(MplsBots));
                        //Console.WriteLine("Bot running at---"+ index.ToString());
                        BOT_LIST.Add(MplsBots.BotName, MplsBots);
                    }
                    //foreach (KeyValuePair<String, Bots> BOT in RUNNING_BOT_LIST) { ScheduleItems.Add(BOT.Value); }
                    //foreach (KeyValuePair<String, Bots> BOT in BOT_LIST) { if (!RUNNING_BOT_LIST.ContainsKey(BOT.Value.BotName)) {  ScheduleItems.Add(BOT.Value); } }
                    Dispatcher.BeginInvoke(new Action(delegate () { this.ScheduleListView.ItemsSource = ScheduleItems; }));
                    Thread.Sleep(950);
                }
            }
            catch (Exception error)
            {
                Dispatcher.BeginInvoke(new Action(delegate () { this.StatusBarContentLabel.Content = "READING BOTS LOG FILE (Exception) : " + error.ToString(); }));
                //MessageBox.Show(error.ToString(), "SCHEDULE MPLS BOTS PROGRAM ERROR",MessageBoxButton.OK, MessageBoxImage.Error);
                //Application.Current.Shutdown();
                InitializeMainThread();
            }
        }
        public void CallToRunPythonBot(object RunningMplsBotObject)
        {
            var RB = (Bots)RunningMplsBotObject;
            try
            {
                if (RUNNING_BOT_LIST.ContainsKey(RB.BotName))
                {
                    Dispatcher.Invoke(new Action(() =>
                    {
                        Thread.Sleep(500);
                        int i = 0;
                        this.CmdPromptViewLabele.AppendText("");
                        var pyProcess = new Process();
                        String[] _PYPATH = File.ReadAllLines("executable.info", System.Text.Encoding.GetEncoding("iso-8859-1"));
                        pyProcess.StartInfo.FileName = @""+ _PYPATH[0].ToString();
                        var RunningPythonPathLocation = @"" + RB.BotPath.ToString();
                        if (File.Exists(RunningPythonPathLocation))
                        {
                            pyProcess.StartInfo.Arguments = $"\"{RunningPythonPathLocation}\"";
                            pyProcess.StartInfo.UseShellExecute = false;
                            pyProcess.StartInfo.RedirectStandardOutput = true;
                            pyProcess.StartInfo.RedirectStandardInput = true;
                            pyProcess.StartInfo.CreateNoWindow = true;
                            pyProcess.OutputDataReceived += new DataReceivedEventHandler((sender, arg) =>
                            {
                                if (RUNNING_BOT_LIST.ContainsKey(RB.BotName.ToString()))
                                {
                                    if (!String.IsNullOrEmpty(arg.Data))
                                    {
                                        this.Dispatcher.Invoke(new Action(() =>
                                        {
                                            if (arg.Data.ToString() == "|END|")
                                            {
                                                try
                                                {
                                                    Dispatcher.Invoke(new Action(() => { this.StatusBarContentLabel.Content = RB.BotName.ToString() + " (CallToRunPythonBot->Handler) : RUNNING BOT IS DONE  . . . "; }));
                                                    Console.WriteLine(i.ToString() + "> (DATA HANDLER) ENDING BOT BY DEFAULT");
                                                    UnLoadingRunningBotToFinish(RB.BotName.ToString());
                                                    pyProcess.Kill();
                                                    pyProcess.Dispose();
                                                }
                                                catch (Exception) { Console.WriteLine(i.ToString() + "> ENDING BOT Exception"); }
                                            }
                                            else
                                            {
                                                Dispatcher.Invoke(new Action(() => { SetTextToCmdView(RB.BotName.ToString(), arg.Data.ToString()); }));
                                                Console.WriteLine(i.ToString() + " > (DATA HANDLER) RECIVING BOT DATA");
                                            }
                                        }));
                                    }
                                    else
                                    { Dispatcher.Invoke(new Action(() => { this.StatusBarContentLabel.Content = RB.BotName.ToString() + " (CallToRunPythonBot->Handler) : RUNNING NULL VALUES . . . "; })); }
                                }
                                else
                                {
                                    try
                                    {
                                        Dispatcher.Invoke(new Action(() => { this.StatusBarContentLabel.Content = RB.BotName.ToString() + " (CallToRunPythonBot->Handler) : RUNNING BOT IS DONE  . . . "; }));
                                        Console.WriteLine(i.ToString() + "> ENDING BOT BY USER");
                                        UnLoadingRunningBotToFinish(RB.BotName.ToString());
                                        pyProcess.Kill();
                                        pyProcess.Dispose();
                                    }
                                    catch (Exception) { Console.WriteLine(i.ToString() + "> ENDING BOT Exception"); }
                                }
                                i++;
                            });
                            pyProcess.Start();
                            pyProcess.BeginOutputReadLine();
                        }
                        else
                        {
                            MessageBox.Show(RunningPythonPathLocation.ToString()+ "\nCan not find the programs in the location.\nPlease check the path location.", "SCHEDULE MPLS BOTS PROGRAM PATH ERROR",MessageBoxButton.OK, MessageBoxImage.Error);
                            UnLoadingRunningBotToFinish(RB.BotName.ToString());
                        }
                    }));
                }
            }
            catch (Exception error)
            {
                Dispatcher.Invoke(new Action(() =>
                { this.StatusBarContentLabel.Content = RunningMplsBotObject.ToString() + "(CallToRunPythonBot->Exception) : " + error.ToString(); }));
            }
        }
        /***********************************************************/
        #endregion


        #region INITIALIZING GRAPHICAL USER INTERFACE DEFINITION
        /***********************************************************/
        /***  INITIALIZING GRAPHICAL USER INTERFACE DEFINITION  ****/
        /***********************************************************/
        private void StartButtonFunction(object sender, RoutedEventArgs e)
        {
            var ButtonTag = ((Button)sender).Tag;
            //Console.Out.WriteLine(ButtonTag.ToString());
            StartBot(ButtonTag.ToString());
        }
        private void StopButtonFunction(object sender, RoutedEventArgs e)
        {
            var tag = ((Button)sender).Tag;
            //Console.Out.WriteLine(tag.ToString());
            UnLoadingRunningBotToFinish(tag.ToString());
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
        }
        private void Button_Click_Two(object sender, RoutedEventArgs e)
        {
        }
        private void Button_Click_Hide(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }
        private void Button_Click_Settings(object sender, RoutedEventArgs e)
        {
            SettingsWindow SetWin = new SettingsWindow();
            SetWin.Show();
        }
        private void CmdPromptView_TextChanged(object sender, TextChangedEventArgs e)
        {
        }
        private void AboutBotButton_Click(object sender, RoutedEventArgs e)
        {
            AboutWindow AbtWin = new AboutWindow();
            AbtWin.Show();
        }
        /***********************************************************/
        #endregion


    }

}

