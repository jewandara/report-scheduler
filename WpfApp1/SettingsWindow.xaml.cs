﻿using System;
using System.Collections.Generic;
using System.Windows;
using Newtonsoft.Json;
using System.IO;
using System.Windows.Controls;
using Microsoft.Win32;
using System.Windows.Media;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindow : Window
    {
        public SettingsWindow()
        {
            InitializeComponent();
            InitializeDataGrid();
            this.BotLabelMessage.Foreground = new SolidColorBrush(BLUE);
            this.BotLabelMessage.Text = "INITIALIZE DATA GRID SUCCESSFULLY.\nPLEASE SELECT THE ROW IN THE GRID TO EDIT.";
            this.BotButtonUpdate.Visibility = Visibility.Hidden;
            this.BotButtonDelete.Visibility = Visibility.Hidden;
        }

        #region INITIALIZING BOT CONFIG SETTING FILE
        /***********************************************************/
        /********* INITIALIZING BOT CONFIG SETTING FILE ************/
        /***********************************************************/

        public class BotSettings
        {
            public string BotId { get; set; }
            public string BotName { get; set; }
            public string RunType { get; set; }
            public string WeekDay { get; set; }
            public string RunDate { get; set; }
            public int RunHour { get; set; }
            public int RunMinit { get; set; }
            public string BotPath { get; set; }
            public string BotDecription { get; set; }
        }

        public int NumberOfBots = 0;
        Color RED = (Color)ColorConverter.ConvertFromString("#f25033");
        Color GREEN = (Color)ColorConverter.ConvertFromString("#07b029");
        Color BLUE = (Color)ColorConverter.ConvertFromString("#4bb8e3");
        String PATH = "bots.lg";

        /***********************************************************/
        #endregion


        #region INITIALIZING BOT FUNCTIONS
        /***********************************************************/
        /************** INITIALIZING BOT FUNCTIONS *****************/
        /***********************************************************/
        private void InitializeDataGrid()
        {
            try
            {
                this.Width = SystemParameters.PrimaryScreenWidth;
                this.Height = SystemParameters.PrimaryScreenHeight;
                this.Topmost = true;
                this.Top = 0;
                this.Left = 0;
                for (int i = 0; i < 25; i++) { this.BotComboBoxDate.Items.Add((i+1).ToString().PadLeft(2, '0')); }
                for (int i = 0; i < 24; i++) { this.BotComboBoxHour.Items.Add(i.ToString().PadLeft(2, '0')); }
                for (int i = 0; i < 60; i++) { this.BotComboBoxMinutes.Items.Add(i.ToString().PadLeft(2, '0')); }
                this.BotComboBoxDate.SelectedIndex = 14;
                this.BotComboBoxHour.SelectedIndex = 0;
                this.BotComboBoxMinutes.SelectedIndex = 30;
                List<BotSettings> users = new List<BotSettings>();
                String[] _BOTS = File.ReadAllLines(PATH, System.Text.Encoding.GetEncoding("iso-8859-1"));
                foreach (String _DATA in _BOTS)
                {
                    BotSettings MplsBots = JsonConvert.DeserializeObject<BotSettings>(_DATA.ToString());
                    if (MplsBots.RunType == "DAY") { MplsBots.RunDate = ""; }
                    if (MplsBots.RunType == "WEEK") { MplsBots.RunDate = ""; }
                    if (MplsBots.RunType == "MONTH") { MplsBots.WeekDay = ""; }
                    users.Add(new BotSettings()
                    {
                        BotId = MplsBots.BotId.ToString().PadLeft(6, '0'),
                        BotName = MplsBots.BotName,
                        RunType = MplsBots.RunType,
                        WeekDay = MplsBots.WeekDay,
                        RunDate = MplsBots.RunDate,
                        RunHour = MplsBots.RunHour,
                        RunMinit = MplsBots.RunMinit,
                        BotPath = MplsBots.BotPath,
                        BotDecription = MplsBots.BotDecription
                    });
                    if (Int16.Parse(MplsBots.BotId.ToString()) > NumberOfBots) { NumberOfBots = Int16.Parse(MplsBots.BotId.ToString()); }
                }
                this.botSettings.ItemsSource = null;
                this.botSettings.ItemsSource = users;
                String[] _PYPATH = File.ReadAllLines("executable.info", System.Text.Encoding.GetEncoding("iso-8859-1"));
                this.BotTextBoxPythonRunPath.Text = _PYPATH[0].ToString();
            }
            catch (Exception error)
            {
                this.BotLabelMessage.Foreground = new SolidColorBrush(RED);
                this.BotLabelMessage.Text = "READING BOTS LOG FILE (Exception) : \n" + error.ToString();
            }
        }
        private void AddNewBot()
        {
            try
            {
                List<BotSettings> users = new List<BotSettings>();
                Boolean IsBotAvalable = false;
                String[] _BOTS = File.ReadAllLines(PATH, System.Text.Encoding.GetEncoding("iso-8859-1"));
                foreach (String _DATA in _BOTS)
                {
                    BotSettings MplsBots = JsonConvert.DeserializeObject<BotSettings>(_DATA.ToString());
                    if ((NumberOfBots + 1) == Int16.Parse(MplsBots.BotId.ToString())) { IsBotAvalable = true; }
                }
                if (IsBotAvalable)
                {
                    this.BotLabelMessage.Foreground = new SolidColorBrush(RED);
                    this.BotLabelMessage.Text = "EXISTING BOT ID FOUND ON THE GRID.\nPLEASE CREATE A NEW BOT ID.";
                }
                else
                {
                    String jsonWriteBot = "{ " +
                        "'BotId':'" + (NumberOfBots + 1).ToString() + "', " +
                        "'BotName':'" + BotTextBoxName.Text.ToString() + "', " +
                        "'RunType':'" + ((ComboBoxItem)BotComboBoxRunType.SelectedItem).Name.ToString() + "', " +
                        "'WeekDay':'" + ((ComboBoxItem)BotComboBoxWeekDay.SelectedItem).Name.ToString() + "', " +
                        "'RunDate':'" + BotComboBoxDate.SelectedItem.ToString() + "', " +
                        "'RunHour':'" + BotComboBoxHour.SelectedItem.ToString() + "', " +
                        "'RunMinit':'" + BotComboBoxMinutes.SelectedItem.ToString() + "', " +
                        "'BotPath':'" + BotTextBoxPath.Text.ToString().Replace("\\", "\\\\") + "', " +
                        "'BotDecription':'" + BotTextBoxDecription.Text.ToString() + "'" +
                        " }";
                    using (StreamWriter SW = File.AppendText(PATH))
                    {
                        SW.WriteLine(jsonWriteBot);
                        this.BotLabelMessage.Foreground = new SolidColorBrush(GREEN);
                        this.BotLabelMessage.Text = "ADDING A NEW BOT SUCCESSFULLY.";
                    }
                }
            }
            catch (Exception error)
            {
                this.BotLabelMessage.Foreground = new SolidColorBrush(RED);
                this.BotLabelMessage.Text = "ADDING A BOT TO LOG FILE (Exception) : \n" + error.ToString();
            }
        }
        private void UpdateSelectedBot()
        {
            try
            {
                List<BotSettings> users = new List<BotSettings>();
                String[] _BOTS = File.ReadAllLines(PATH, System.Text.Encoding.GetEncoding("iso-8859-1"));
                File.WriteAllText(PATH, String.Empty);
                foreach (String _DATA in _BOTS)
                {
                    BotSettings MplsBots = JsonConvert.DeserializeObject<BotSettings>(_DATA.ToString());
                    if (Int16.Parse(MplsBots.BotId) == Int16.Parse(BotTextBoxId.Text))
                    {
                        String jsonWriteBot = "{ " +
                            "'BotId':'" + BotTextBoxId.Text.ToString() + "', " +
                            "'BotName':'" + BotTextBoxName.Text.ToString() + "', " +
                            "'RunType':'" + ((ComboBoxItem)BotComboBoxRunType.SelectedItem).Name.ToString() + "', " +
                            "'WeekDay':'" + ((ComboBoxItem)BotComboBoxWeekDay.SelectedItem).Name.ToString() + "', " +
                            "'RunDate':'" + BotComboBoxDate.SelectedItem.ToString() + "', " +
                            "'RunHour':'" + BotComboBoxHour.SelectedItem.ToString() + "', " +
                            "'RunMinit':'" + BotComboBoxMinutes.SelectedItem.ToString() + "', " +
                            "'BotPath':'" + BotTextBoxPath.Text.ToString().Replace("\\", "\\\\") + "', " +
                            "'BotDecription':'" + BotTextBoxDecription.Text.ToString() + "'" +
                            " }";
                        using (StreamWriter SW = File.AppendText(PATH)) { SW.WriteLine(jsonWriteBot); }
                    }
                    else
                    {
                        String jsonWriteBot = "{ " +
                            "'BotId':'" + MplsBots.BotId.ToString() + "', " +
                            "'BotName':'" + MplsBots.BotName.ToString() + "', " +
                            "'RunType':'" + MplsBots.RunType.ToString() + "', " +
                            "'WeekDay':'" + MplsBots.WeekDay.ToString() + "', " +
                            "'RunDate':'" + MplsBots.RunDate.ToString() + "', " +
                            "'RunHour':'" + MplsBots.RunHour.ToString() + "', " +
                            "'RunMinit':'" + MplsBots.RunMinit.ToString() + "', " +
                            "'BotPath':'" + MplsBots.BotPath.ToString().Replace("\\", "\\\\") + "', " +
                            "'BotDecription':'" + MplsBots.BotDecription.ToString() + "'" +
                            " }"; 
                        using (StreamWriter SW = File.AppendText(PATH)) { SW.WriteLine(jsonWriteBot); }
                    }
                }
                this.BotLabelMessage.Foreground = new SolidColorBrush(GREEN);
                this.BotLabelMessage.Text = "UPDATING SELECTED BOT IS SUCCESSFULLY.";
            }
            catch (Exception error) { 
                this.BotLabelMessage.Foreground = new SolidColorBrush(RED); 
                this.BotLabelMessage.Text = "UPDATING BOTS LOG FILE (Exception) : \n" + error.ToString(); 
            }
        }
        private void DeleteSelectedBot()
        {
            try
            {
                if ((Int16.Parse(BotTextBoxId.Text) != 0) || (BotTextBoxId.Text != null))
                {
                    List<BotSettings> users = new List<BotSettings>();
                    String[] _BOTS = File.ReadAllLines(PATH, System.Text.Encoding.GetEncoding("iso-8859-1"));
                    File.WriteAllText(PATH, String.Empty);
                    foreach (String _DATA in _BOTS)
                    {
                        BotSettings MplsBots = JsonConvert.DeserializeObject<BotSettings>(_DATA.ToString());
                        if (Int16.Parse(MplsBots.BotId) == Int16.Parse(BotTextBoxId.Text))
                        {
                            this.BotLabelMessage.Foreground = new SolidColorBrush(GREEN);
                            this.BotLabelMessage.Text = "BOT LOG FILE DELETING IS SUCCESSFULLY";
                        }
                        else
                        {
                            String jsonWriteBot = "{ " +
                                "'BotId':'" + MplsBots.BotId.ToString() + "', " +
                                "'BotName':'" + MplsBots.BotName.ToString() + "', " +
                                "'RunType':'" + MplsBots.RunType.ToString() + "', " +
                                "'WeekDay':'" + MplsBots.WeekDay.ToString() + "', " +
                                "'RunDate':'" + MplsBots.RunDate.ToString() + "', " +
                                "'RunHour':'" + MplsBots.RunHour.ToString() + "', " +
                                "'RunMinit':'" + MplsBots.RunMinit.ToString() + "', " +
                                "'BotPath':'" + MplsBots.BotPath.ToString().Replace("\\", "\\\\") + "', " +
                                "'BotDecription':'" + MplsBots.BotDecription.ToString() + "'" +
                                " }";
                            using (StreamWriter SW = File.AppendText(PATH)) { SW.WriteLine(jsonWriteBot); }
                        }
                    }
                }
            }
            catch (Exception error) { this.BotLabelMessage.Foreground = new SolidColorBrush(RED); this.BotLabelMessage.Text = "DELETING BOTS LOG FILE (Exception) : \n" + error.ToString(); }
        }
        private void CleardBotData()
        {
            this.BotTextBoxId.Text = "00000";
            this.BotTextBoxName.Text = "";
            this.BotComboBoxRunType.SelectedIndex = 1;
            this.BotComboBoxWeekDay.SelectedIndex = 6;
            this.BotComboBoxDate.SelectedIndex = 14;
            this.BotComboBoxHour.SelectedIndex = 12;
            this.BotComboBoxMinutes.SelectedIndex = 30;
            this.BotTextBoxPath.Text = "";
            this.BotTextBoxDecription.Text = "";
            this.BotButtonAdd.Visibility = Visibility.Visible;
            this.BotButtonUpdate.Visibility = Visibility.Hidden;
            this.BotButtonDelete.Visibility = Visibility.Hidden;
        }
        private void CheckRunTypeBot()
        {
            try
            {
                if (((ComboBoxItem)BotComboBoxRunType.SelectedItem).Name.ToString() == "DAY")
                {
                    this.BotComboBoxDate.Visibility = Visibility.Hidden;
                    this.BotLabelDate.Visibility = Visibility.Hidden;
                    this.BotComboBoxWeekDay.Visibility = Visibility.Visible;
                }
                else if (((ComboBoxItem)BotComboBoxRunType.SelectedItem).Name.ToString() == "WEEK")
                {
                    this.BotComboBoxDate.Visibility = Visibility.Hidden;
                    this.BotLabelDate.Visibility = Visibility.Hidden;
                    this.BotComboBoxWeekDay.Visibility = Visibility.Visible;
                }
                else if (((ComboBoxItem)BotComboBoxRunType.SelectedItem).Name.ToString() == "MONTH")
                {
                    this.BotComboBoxDate.Visibility = Visibility.Visible;
                    this.BotLabelDate.Visibility = Visibility.Visible;
                    this.BotComboBoxWeekDay.Visibility = Visibility.Hidden;
                }
                else
                {
                    this.BotComboBoxDate.Visibility = Visibility.Visible;
                    BotLabelDate.Visibility = Visibility.Visible;
                    this.BotComboBoxWeekDay.Visibility = Visibility.Visible;
                }
            }
            catch (Exception){}
        }
        private void botSettings_MouseDoubleClick(object sender, RoutedEventArgs e)
        {
            if (botSettings != null && botSettings.SelectedItems != null && botSettings.SelectedItems.Count == 1)
            {
                DataGridRow DataGridRowBotSettings = botSettings.ItemContainerGenerator.ContainerFromItem(botSettings.SelectedItem) as DataGridRow;
                if (DataGridRowBotSettings != null)
                {
                    var data = DataGridRowBotSettings.Item as BotSettings;
                    this.BotTextBoxName.Text = data.BotName;
                    this.BotTextBoxPath.Text = data.BotPath;
                    this.BotTextBoxDecription.Text = data.BotDecription;
                    this.BotTextBoxId.Text = data.BotId;
                    if (data.RunType == "DAY") { this.BotComboBoxRunType.SelectedIndex = 0; }
                    if (data.RunType == "WEEK") { this.BotComboBoxRunType.SelectedIndex = 1; }
                    if (data.RunType == "MONTH") { this.BotComboBoxRunType.SelectedIndex = 2; }
                    if (data.WeekDay == "MONDAY") { this.BotComboBoxWeekDay.SelectedIndex = 0; }
                    if (data.WeekDay == "TUESDAY") { this.BotComboBoxWeekDay.SelectedIndex = 1; }
                    if (data.WeekDay == "WEDNESDAY") { this.BotComboBoxWeekDay.SelectedIndex = 2; }
                    if (data.WeekDay == "THURSDAY") { this.BotComboBoxWeekDay.SelectedIndex = 3; }
                    if (data.WeekDay == "FRIDAY") { this.BotComboBoxWeekDay.SelectedIndex = 4; }
                    if (data.WeekDay == "SATURDAY") { this.BotComboBoxWeekDay.SelectedIndex = 5; }
                    if (data.WeekDay == "SUNDAY") { this.BotComboBoxWeekDay.SelectedIndex = 6; }
                    if (data.WeekDay == "") { this.BotComboBoxWeekDay.SelectedIndex = 6; }

                    if (data.RunDate.ToString() == "") { this.BotComboBoxDate.SelectedIndex = 14;  }
                    else { this.BotComboBoxDate.SelectedIndex = Int16.Parse(data.RunDate.ToString()) - 1; }

                    this.BotComboBoxHour.SelectedIndex = Int16.Parse(data.RunHour.ToString());
                    this.BotComboBoxMinutes.SelectedIndex = Int16.Parse(data.RunMinit.ToString());
                    this.BotButtonAdd.Visibility = Visibility.Hidden;
                    this.BotButtonUpdate.Visibility = Visibility.Visible;
                    this.BotButtonDelete.Visibility = Visibility.Visible;
                }
                else { this.BotLabelMessage.Foreground = new SolidColorBrush(RED); this.BotLabelMessage.Text = "NO SELECTED ITEM FOUND ON THE GRID.\nPLEASE SELECT AN ITEM FROM THE GRID."; }
            }
        }
        private void BotComboBoxRunType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CheckRunTypeBot();
        }

        /***********************************************************/
        #endregion


        #region BOT SETTING FILE FUNCTIONS
        /***********************************************************/
        /************* BOT CONFIGERATION FUNCTIONS *****************/
        /***********************************************************/
        private void BotButtonClear_Click(object sender, RoutedEventArgs e)
        {
            CleardBotData();
            this.BotLabelMessage.Foreground = new SolidColorBrush(BLUE);
            this.BotLabelMessage.Text = "PLEASE SELECT THE ROW IN THE GRID TO EDIT.";
        }
        private void BotButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            this.BotLabelMessage.Foreground = new SolidColorBrush(RED);
            if ((Int16.Parse(BotTextBoxId.Text) == 0) || (BotTextBoxId.Text == ""))
            {
                if (BotTextBoxName.Text != "")
                {
                    if (File.Exists(BotTextBoxPath.Text))
                    {
                        AddNewBot();
                        InitializeDataGrid();
                        CleardBotData();
                    }
                    else { this.BotLabelMessage.Text = "CAN NOT FIND THE BOT RUNNING LOCATION PATH.\nPLEASE BROWSE THE PYTHON BOT LOCATION."; }
                }
                else { this.BotLabelMessage.Text = "BOT NAME CANNOT HAVE AN EMPTY NAME.\nPLEASE FILL THE BOT NAME FIELD.";  }
            }
            else { this.BotLabelMessage.Text = "THIS BOT ID CANNOT BE ADDED TO CONFIG.\nPLEASE CLEAR THE EXISTING DATA."; }
        }
        private void BotButtonUpdate_Click(object sender, RoutedEventArgs e)
        {
            this.BotLabelMessage.Foreground = new SolidColorBrush(RED);
            if ((Int16.Parse(BotTextBoxId.Text) != 0) || (BotTextBoxId.Text != ""))
            {
                if (BotTextBoxName.Text != "")
                {
                    if (File.Exists(BotTextBoxPath.Text))
                    {
                        UpdateSelectedBot();
                        InitializeDataGrid();
                        CleardBotData();
                    }
                    else { this.BotLabelMessage.Text = "CAN NOT FIND THE BOT RUNNING LOCATION PATH.\nPLEASE BROWSE THE PYTHON BOT LOCATION."; }
                }
                else { this.BotLabelMessage.Text = "BOT NAME CANNOT HAVE AN EMPTY NAME.\nPLEASE FILL THE BOT NAME FIELD."; }
            }
            else { this.BotLabelMessage.Text = "CAN NOT FIND THE SELECTED BOT ID.\nPLEASE SELECT THE BOT FROM THE GRID."; }
        }
        private void BotButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            DeleteSelectedBot();
            InitializeDataGrid();
            CleardBotData();
        }
        private void BotButtonBrowse_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Python Files (*.py, *.pyc, *.txt, *.exe) | *.py; *.pyc; *.txt; *.exe";
            openFileDialog.Title = "Please select python bot executable file. ( main.py, _init_.py )";
            if (openFileDialog.ShowDialog() == true) { this.BotTextBoxPath.Text = openFileDialog.FileName; }
        }
        private void BotButtonClose_Click(object sender, RoutedEventArgs e)
        { this.Close(); }
        private void BotButtonChange_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Python Files (*.py, *.pyc, *.exe) | *.py; *.pyc; *.exe";
            openFileDialog.Title = "Please Executable file. ( python.exe )";
            if (openFileDialog.ShowDialog() == true) {
                File.WriteAllText("executable.info", openFileDialog.FileName.ToString());
                this.BotTextBoxPythonRunPath.Text = openFileDialog.FileName; 
            }
        }

        /***********************************************************/
        #endregion

    }
}
